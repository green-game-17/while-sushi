extends Path2D


signal passed_sushi(sushi)


export (PackedScene) var beltObject
const BELT_SPEED = 40
const MAX_LIST_LENGTH = 100
const ENTRY_POINT = 1700

var objectList = []
var beltPosition = 300
var beltLength = self.curve.get_baked_length()
var playerNear = false


# Called when the node enters the scene tree for the first time.
func _ready():
	self.connect('passed_sushi', self, 'add_new_object')
	EventDominator.connect('requested_clear_belt', self, '__on_clear_belt')


func _input(event):
	if Input.is_action_pressed("ui_select") && self._has_free_space() && playerNear:
		EventDominator.emit_signal('reqeusted_pass_sushi', self)
	#if Input.is_action_pressed("add_new"):
		#self.add_new_object('sth')


func add_new_object(sushi):
	var obj = beltObject.instance()
	obj.sushi = sushi
	obj.offset = self._get_belt_object_offset()
	objectList.append(obj)
	obj.set_connection(self)
	self.add_child(obj)
	$Sound.play()


func _has_free_space() -> bool:
	if objectList.size() == MAX_LIST_LENGTH:
		return false
	
	var beltFraction = beltLength / MAX_LIST_LENGTH
	var maxi = beltLength
	var mini = 0
	for object in objectList:
		if object.offset > mini && object.offset < ENTRY_POINT:
			mini = object.offset
		if object.offset < maxi && object.offset > ENTRY_POINT:
			maxi = object.offset
	if maxi < ENTRY_POINT + beltFraction:
		if not abs(mini - (maxi - beltFraction)) < 1:
			return true
	elif mini > ENTRY_POINT - beltFraction:
		if not abs(maxi - (mini + beltFraction)):
			return true
	else:
		return true
	return false


func _get_belt_object_offset() -> float:
	var beltFraction = beltLength / MAX_LIST_LENGTH
	var maxi = beltLength
	var mini = 0
	for object in objectList:
		if object.offset > mini && object.offset < ENTRY_POINT:
			mini = object.offset
		if object.offset < maxi && object.offset > ENTRY_POINT:
			maxi = object.offset
	if maxi < ENTRY_POINT + beltFraction:
		return maxi - beltFraction
	elif mini > ENTRY_POINT - beltFraction:
		return mini + beltFraction
	else:
		var startPoint = beltPosition
		while startPoint < ENTRY_POINT - beltFraction || startPoint > ENTRY_POINT + beltFraction:
			startPoint += beltFraction
			if startPoint > beltLength:
				startPoint -= beltLength
		return startPoint


func _physics_process(delta):
	var distance = BELT_SPEED * delta
	beltPosition += distance
	if beltPosition > beltLength:
		beltPosition -= beltLength
	for obj in objectList:
		obj.offset += distance


func _on_Player_exited(area):
	var player = area.owner
	if player.name == 'Player':
		playerNear = false
		$Selection.hide()


func _on_Player_entered(area):
	var player = area.owner
	if player.name == 'Player':
		playerNear = true
		$Selection.show()


func _on_belt_object_is_deleted(belt_object):
	objectList.erase(belt_object)
	belt_object.queue_free()

	
func __on_clear_belt():
	for obj in objectList:
		obj.delete_this()  # just DO IT!
