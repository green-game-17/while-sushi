extends '../work_counter.gd'


func _ready():
	anchors = [
		$Anchor1, $Anchor2, $Anchor3, $Anchor4
	]

func _on_passed_plate(plate):
	if not plate:
		return

	if not 'riceball' in plate.ingredients:
		__deny_plate("Hmh, you can't prepare sushi without rice! Go grab some.", plate)
		return

	if not 'nori' in plate.ingredients:
		__deny_plate("Hmh, you can't prepare sushi without nori! Go grab some.", plate)
		return

	var dish_ingredients = []
	for ingredient in plate.ingredients:
		if not ingredient in ['riceball', 'nori']:
			dish_ingredients.append(ingredient)
	
	if dish_ingredients.empty():
		__deny_plate("Hmh, you can't prepare sushi without filling! Go grab some.", plate)
		return

	emit_signal('acknowledged_action', 'add_ingredient')

	if dish_ingredients.size() == 3:
		if dish_ingredients[0] == dish_ingredients[1] && dish_ingredients[1] == dish_ingredients[2]:
			for a in anchors:
				var new_sushi = sushi_scene.instance()
				new_sushi.prepare_super_size('maki', dish_ingredients[0], true)
				a.add_child(new_sushi)
				sushis.append(new_sushi)
			return

	var return_ingredients = []

	var main_ingredient = dish_ingredients[0]
	var side_ingredient_1 = null
	var side_ingredient_2 = null
	
	if dish_ingredients.size() > 1:
		if dish_ingredients[1] != dish_ingredients[0]:
			side_ingredient_1 = dish_ingredients[1]
		else:
			return_ingredients.append(dish_ingredients[1])
	if dish_ingredients.size() == 3:
		if dish_ingredients[2] != dish_ingredients[1] && dish_ingredients[2] != dish_ingredients[0]:
			side_ingredient_2 = dish_ingredients[2]
		else:
			return_ingredients.append(dish_ingredients[2])
	
	if not side_ingredient_1 and side_ingredient_2:
		side_ingredient_1 = side_ingredient_2
		side_ingredient_2 = null

	for a in anchors:
		var new_sushi = sushi_scene.instance()
		new_sushi.prepare('maki',
			main_ingredient, side_ingredient_1, side_ingredient_2, true
		)
		a.add_child(new_sushi)
		sushis.append(new_sushi)

	if not return_ingredients.empty():
		plate.re_set_ingredients(return_ingredients)
		EventDominator.emit_signal('requested_swap_plate', plate, null)
