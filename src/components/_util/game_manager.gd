extends Node


signal game_finished


const MAX_RATINGS = 50
const SPAWN_RATE = 4000


onready var customer_scene = load('res://components/customer/customer.tscn')


var is_active
var past_ratings
var overall_satisfaction
var overall_rating
var overall_money

var debug = true


# Init Methods

func _ready():
	randomize()
	_set_base_values()
	__print_debug()


func _set_base_values():
	is_active = false
	past_ratings = [2.5]
	overall_satisfaction = 0.0
	overall_rating = 0.0
	overall_money = 400


# State Methods
func activate():
	_set_base_values()
	is_active = true


func deactivate():
	is_active = false


# Getter Methods

func get_current_rating() -> float:
	return overall_rating


func get_current_satisfaction() -> float:
	return overall_satisfaction


func get_current_money() -> int:
	return overall_money


func has_enough_money(amount) -> bool:
	return overall_money >= amount


# Setter Methods

func add_rating(new_rating):
	past_ratings.push_front(new_rating)
	if past_ratings.size() > MAX_RATINGS:
		past_ratings = past_ratings.slice(0, 49)
	__print_debug()


func reduce_money(amount):
	overall_money -= int(amount)
	__print_debug()


func add_money(amount):
	overall_money += int(amount)
	__print_debug()


# Lööp Methods

func _physics_process(_delta):
	if is_active:
		var tables = get_tree().get_nodes_in_group('table')
		var satisfaction = 0.0
		var rating = 0.0
		var losing_rating = 0.0
		
		if randi() % SPAWN_RATE < get_current_rating():
			var free_tables = []
			for table in tables:
				if table.has_free_seats():
					free_tables.append(table)
			if free_tables.size() > 0:
				free_tables[randi() % free_tables.size()]._on_new_customer(customer_scene.instance())
		
		var table_count = 0
		for table in tables:
			var satisfaction_level = table.get_satisfaction_level()
			if satisfaction_level >= 0:
				satisfaction += satisfaction_level
				table_count += 1
		if table_count > 0:
			overall_satisfaction = (satisfaction / table_count) / 16
		else:
			overall_satisfaction = 1
		
		for entry in past_ratings:
			rating += entry
		overall_rating = rating / past_ratings.size()
		
		for pos in 20:
			if pos < past_ratings.size():
				losing_rating += past_ratings[pos]
		if losing_rating / min(past_ratings.size(), 20) == 1:
			deactivate()
			emit_signal("game_finished")


# Utility Methods

func __print_debug():
	if debug:
		print('[GM]: changed - current state: %s' % self)


func _to_string():
	return '<is_active=%s , past_ratings=%s , overall_satisfaction=%s , overall_rating=%s , overall_money=%s>' % [
		is_active, past_ratings, overall_satisfaction, overall_rating, overall_money
	]
