extends Node2D


var debugSignals = true


# UI
signal added_nicky_message(text)
signal added_loop_message(text)

# Plate interaction
signal requested_start_plate(sender)
signal requested_pass_plate(target)
signal requested_add_ingredient(ingredient, sender)
signal requested_remove_ingredient(ingredient, sender)
signal requested_swap_plate(plate, sender)
signal reqeusted_pick_sushi(sender)
signal reqeusted_pass_sushi(target)
signal requested_swap_carry(object, sender)
signal requested_clear_belt()


func _ready():
	if debugSignals:
		print('I hope you like debug prints, cause there are many here')		
		for s in self.get_signal_list():
			if s.args.empty():
				connect(s.name, self, "__on_signal_no_args", [s.name])
			elif s.args.size() == 1:
				connect(s.name, self, "__on_signal_one_args", [s.name])
			elif s.args.size() == 2:
				connect(s.name, self, "__on_signal_two_args", [s.name])
			else:
				push_error(
					'[ED]! %s name has more than 2 args, debug not set up!' % s.name
				)


func __on_signal_no_args(name):
	print('[ED]: got signal %s' % name)


func __on_signal_one_args(arg, name):
	print('[ED]: got signal %s with arg %s' % [name, arg])


func __on_signal_two_args(arg1, arg2, name):
	print('[ED]: got signal %s with args %s , %s' % [name, arg1, arg2])
