extends Control


func _ready():
	get_node('CenterContainer/VBoxContainer/MoneyLabel').text = str(
		GameManager.get_current_money()
	)

	get_node('CenterContainer/VBoxContainer/HBoxContainer/AgainButton').connect(
		'gui_input', self, '__on_again'
	)
	get_node('CenterContainer/VBoxContainer/HBoxContainer/MenuButton').connect(
		'gui_input', self, '__on_menu'
	)


func __on_again(event):
	if event.is_pressed():
		SceneDominator.goto_scene('world')


func __on_menu(event):
	if event.is_pressed():
		SceneDominator.goto_scene('menu')
