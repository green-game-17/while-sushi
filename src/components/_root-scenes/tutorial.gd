extends Node2D


onready var MARKERS = {
	'satisfaction': get_node('YSort/Player/Camera2D/HUD/MarkerSatisfaction'),
	'rating': get_node('YSort/Player/Camera2D/HUD/MarkerRating'),
	'money': get_node('YSort/Player/Camera2D/HUD/MarkerMoney'),
	'plate': get_node('YSort/KitchenMarkers/MarkerPlate'),
	'belt': get_node('YSort/KitchenMarkers/MarkerBelt'),
	'nori': get_node('YSort/KitchenMarkers/MarkerNori'),
	'rice': get_node('YSort/KitchenMarkers/MarkerRice'),
	'tuna': get_node('YSort/KitchenMarkers/MarkerTuna'),
	'salmon': get_node('YSort/KitchenMarkers/MarkerSalmon'),
	'pepper': get_node('YSort/KitchenMarkers/MarkerPepper'),
	'cucumber': get_node('YSort/KitchenMarkers/MarkerCucumber'),
	'nigiri': get_node('YSort/KitchenMarkers/MarkerNigiri'),
	'maki': get_node('YSort/KitchenMarkers/MarkerMaki')
}
onready var STEPS = [
	{
		'text': "Well, i am glad you finally showed up. We need to start making sushi as fast as possible.",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "Ohh wait... You don't know how to make sushi? Well, let me explain it to you.",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "First things first, you can move around by using the WASD or arrow keys.",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "Lööp cat over there is asking for sushi, so let's give him some.",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "To make sushi we first need a plate. Go and grab one from the stack over there by pressing space.",
		'is_loop_cat': false,
		'marker': MARKERS.get('plate'),
		'advance_condition': 'got_plate',
		'extra_action': null
	},
	{
		'text': "Now let's start with the essentials. Start by taking nori and put it on your plate.",
		'is_loop_cat': false,
		'marker': MARKERS.get('nori'),
		'advance_condition': 'got_nori',
		'extra_action': null
	},
	{
		'text': "For sushi you also need some rice, go and get some out of the tray over there.",
		'is_loop_cat': false,
		'marker': MARKERS.get('rice'),
		'advance_condition': 'got_riceball',
		'extra_action': null
	},
	{
		'text': "Each catstomer has different taste, so let's just ask what lööp wants.",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{	
		'text': "I prefer it when my sushi is mainly tuna with a little cucumber and pepper.",
		'is_loop_cat': true,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "Alright, sounds good. To use tuna as the main ingredient it has to be the first ingredient we pick up.",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "So let's get some fresh tuna out of the freezer.",
		'is_loop_cat': false,
		'marker': MARKERS.get('tuna'),
		'advance_condition': 'got_tuna',
		'extra_action': null
	},
	{
		'text': "Now take a cucumber from over there as the second ingredient.",
		'is_loop_cat': false,
		'marker': MARKERS.get('cucumber'),
		'advance_condition': 'got_cucumber',
		'extra_action': null
	},
	{
		'text': "For the last ingredient you just need to grab a bell pepper from there.",
		'is_loop_cat': false,
		'marker': MARKERS.get('pepper'),
		'advance_condition': 'got_pepper',
		'extra_action': null
	},
	{
		'text': "The order for the side ingredients doesn't matter. You can use none, one or two side ingredients in a maki sushi.",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "Ok, we've got all the ingredients. Let us go over to the working station over there in order to make the sushi.",
		'is_loop_cat': false,
		'marker': MARKERS.get('maki'),
		'advance_condition': 'prepare_maki',
		'extra_action': null
	},
	{
		'text': "After the sushi is done, you can pick it up by pressing space.",
		'is_loop_cat': false,
		'marker': MARKERS.get('maki'),
		'advance_condition': 'pick_sushi',
		'extra_action': null
	},
	{
		'text': "Now you just have to carry it over to the belt over there and press space to put it on.",
		'is_loop_cat': false,
		'marker': MARKERS.get('belt'),
		'advance_condition': 'sushi_on_belt',
		'extra_action': null
	},
	{
		'text': "After it travelled over to lööp he will take it off the belt and eat it.",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{	
		'text': "Purrrrfect! Just as I dreamt öf...",
		'is_loop_cat': true,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': 'remove_sushi_from_belt'
	},
	#{	
	#	'text': "Well, it is nöt what i ördered, but it is still sushi",
	#	'is_loop_cat': true,
	#	'marker': null,
	#	'advance_condition': 'timer',
	#	'extra_action': null
	#},
	{
		'text': "Depending on the taste of the catstomer and the sushi they got, their satisfaction changes.",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "The catstomers also don't like to wait for too long, so keep in mind to work fast.",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "You can have a look at the current satisfaction in the restaurant in the top left.",
		'is_loop_cat': false,
		'marker': MARKERS.get('satisfaction'),
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "Red means bad, green is good. Kind of self explanatory.",
		'is_loop_cat': false,
		'marker': MARKERS.get('satisfaction'),
		'advance_condition': 'timer',
		'extra_action': null
	},
	{	
		'text': "Thank yöu, it was really gööd, but can i have sömething with önly tuna nöw.",
		'is_loop_cat': true,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "Nigiri is just with one ingredient, so let's make some. It is pretty similar to maki. Let's start with a plate again.",
		'is_loop_cat': false,
		'marker': MARKERS.get('plate'),
		'advance_condition': 'got_plate',
		'extra_action': null
	},
	{
		'text': "For nigiri you need rice, so let's get some.",
		'is_loop_cat': false,
		'marker': MARKERS.get('rice'),
		'advance_condition': 'got_riceball',
		'extra_action': null
	},
	{
		'text': "Alright, now get some tuna for the nigiri out of the freezer.",
		'is_loop_cat': false,
		'marker': MARKERS.get('tuna'),
		'advance_condition': 'got_tuna',
		'extra_action': null
	},
	{
		'text': "Now you just carry it over to the cutting board, to make nigiri out of it by pressing space again.",
		'is_loop_cat': false,
		'marker': MARKERS.get('nigiri'),
		'advance_condition': 'prepare_nigiri',
		'extra_action': null
	},
	{
		'text': "You need to pick it up now and then put it on the belt.",
		'is_loop_cat': false,
		'marker': MARKERS.get('belt'),
		'advance_condition': 'sushi_on_belt',
		'extra_action': null
	},
	{
		'text': "Lööp will take it again and let's hear what he says.",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{	
		'text': "Nice, yöu put tuna ön rice. I really löve that cömpösitiön.",
		'is_loop_cat': true,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': 'remove_sushi_from_belt'
	},
	#{	
	#	'text': "Mhhh. This döes nöt löök like tuna, but i will still eat it.",
	#	'is_loop_cat': true,
	#	'marker': null,
	#	'advance_condition': 'timer',
	#	'extra_action': null
	#},
	{	
		'text': "That was deliciöus. Well, i actually am pretty stuffed nöw. I will pröbably leave a review för this place.",
		'is_loop_cat': true,
		'marker': MARKERS.get('review'),
		'advance_condition': 'timer',
		'extra_action': null
	},
	{	
		'text': "There you gö here is your möney, bröther.",
		'is_loop_cat': true,
		'marker': MARKERS.get('money'),
		'advance_condition': 'timer',
		'extra_action': 'pay'
	},
	{	
		'text': "Göödbye, i will cöme again.",
		'is_loop_cat': true,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "In order to keep the business running we need those reviews lööp mentioned.",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': 'yeet_loop_cat'
	},
	{
		'text': "More catstomers will come if we have a high rating, which means more profits!",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "But we have to satisfy our catstomers, because they pay according to their satisfaction.",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "If you ever go down to zero stars, you will be fired. No pressure though.",
		'is_loop_cat': false,
		'marker': MARKERS.get('review'),
		'advance_condition': 'timer',
		'extra_action': null
	},
	{
		'text': "Well, but don't worry about that for now, just stick with making catstomers happy.",
		'is_loop_cat': false,
		'marker': null,
		'advance_condition': 'timer',
		'extra_action': null
	}
]

onready var timer = get_node('Timer')
onready var loop_cat = get_node('YSort/TutorialExtras/LoopSprite')

var current_marker = null
var current_advance_condition = 'timer'
var step_index = -1


func _ready():
	timer.connect('timeout', self, '__on_timeout')
	EventDominator.connect('requested_start_plate', self, '__on_start_plate')#(sender)
	EventDominator.connect('requested_pass_plate', self, '__on_pass_plate')#(target)
	EventDominator.connect('requested_add_ingredient', self, '__on_add_ingredient')#(ingredient, sender)
	EventDominator.connect('reqeusted_pick_sushi', self, '__on_pick_sushi')#(sender)
	EventDominator.connect('reqeusted_pass_sushi', self, '__on_pass_sushi')#(target)

	for marker_key in MARKERS:
		MARKERS.get(marker_key).visible = false

	__advance()


func __on_timeout():
	if current_advance_condition == 'timer':
		__advance()


func __on_start_plate(_s):
	if current_advance_condition == 'got_plate':
		__advance()


func __on_pass_plate(target):
	if target.name == 'MakiCounter' and current_advance_condition == 'prepare_maki':
		__advance()
	elif target.name == 'NigiriCounter' and current_advance_condition == 'prepare_nigiri':
		__advance()


func __on_add_ingredient(ingredient, _s):
	if current_advance_condition == 'got_%s' % ingredient:
		__advance()


func __on_pick_sushi(_s):
	if current_advance_condition == 'pick_sushi':
		__advance()


func __on_pass_sushi(target):
	if target.name == 'SushiBelt' and current_advance_condition == 'sushi_on_belt':
		__advance()


func __advance():
	step_index += 1
	if step_index == STEPS.size():
		SceneDominator.goto_scene('menu')
		return

	var current_step = STEPS[step_index]

	var extra_action = current_step.get('extra_action')
	if extra_action == 'remove_sushi_from_belt':
		EventDominator.emit_signal('requested_clear_belt')
	elif extra_action == 'pay':
		GameManager.add_money(42)
	elif extra_action == 'yeet_loop_cat':
		loop_cat.visible = false

	var msg_signal = 'added_nicky_message'
	if current_step.get('is_loop_cat'):
		msg_signal = 'added_loop_message'
	EventDominator.emit_signal(msg_signal, current_step.get('text'))

	current_advance_condition = current_step.get('advance_condition')
	if current_advance_condition == 'timer':
		timer.start()

	if current_marker:
		current_marker.visible = false
	current_marker = current_step.get('marker')
	if current_marker:
		current_marker.visible = true
