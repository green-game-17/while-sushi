extends KinematicBody2D


export var speed = 250


onready var sprite = $Sprite


var texture_up = load("res://assets/player/back.png")
var texture_down = load("res://assets/player/front.png")
var texture_left = load("res://assets/player/side_left.png") 
var texture_right = load("res://assets/player/side_right.png") 


func _physics_process(delta):
	var velocity = __calc_velocity_for_input_and_move(delta)
	__update_texture_for_velocity(velocity)
	__update_anchor_for_velocity(velocity)


func __calc_velocity_for_input_and_move(delta):
	var total_velocity = Vector2()
	var velocity_did_apply

	var strafe_velocity = Vector2()
	if Input.is_action_pressed('ui_right'):
		strafe_velocity.x += 1
	if Input.is_action_pressed('ui_left'):
		strafe_velocity.x -= 1
	
	__move_for_velocity(strafe_velocity, delta)
	total_velocity.x = strafe_velocity.x
	
	var gliding_velocity = Vector2()
	if Input.is_action_pressed('ui_down'):
		gliding_velocity.y += 1
	if Input.is_action_pressed('ui_up'):
		gliding_velocity.y -= 1

	__move_for_velocity(gliding_velocity, delta)
	total_velocity.y = gliding_velocity.y
	
	return total_velocity.normalized()


func __move_for_velocity(velocity, delta):
	velocity = velocity.normalized() * speed
	move_and_collide(velocity * delta)


func __update_texture_for_velocity(velocity):
	if velocity.x < 0:
		sprite.texture = texture_left
	elif velocity.x > 0:
		sprite.texture = texture_right
	elif velocity.y < 0:
		sprite.texture = texture_up
	elif velocity.y > 0:
		sprite.texture = texture_down


func __update_anchor_for_velocity(_velocity):
	pass
