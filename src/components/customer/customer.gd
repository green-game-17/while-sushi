class_name Customer
extends Sprite


const SATISFACTION_MAX = 80
const SATISFACTION_SAD = 15
const SATISFACTION_ANGRY = 8
const MIN_AMOUNT_TO_EAT = 3
const MAX_AMOUNT_TO_EAT = 6
const INGREDIENT_NAMES = [
	'salmon',
	'shrimp',
	'squid',
	'tuna',
	'carrot',
	'avacado',
	'cucumber',
	'pepper'
]
var INGREDIENT_DATA = {
	'salmon': {
		'texture': load("res://assets/food/raw/salmon.png"),
		'height': 9,
		'offset': 10
	},
	'shrimp': {
		'texture': load("res://assets/food/raw/shrimp.png"),
		'height': 7,
		'offset': 9
	},
	'squid': {
		'texture': load("res://assets/food/raw/squid.png"),
		'height': 9,
		'offset': 10
	},
	'tuna': {
		'texture': load("res://assets/food/raw/tuna.png"),
		'height': 10,
		'offset': 10
	},
	'carrot': {
		'texture': load("res://assets/food/raw/carrot.png"),
		'height': 4,
		'offset': 8
	},
	'avacado': {
		'texture': load("res://assets/food/raw/avacado.png"),
		'height': 19,
		'offset': 8
	},
	'cucumber': {
		'texture': load("res://assets/food/raw/cucumber.png"),
		'height': 5,
		'offset': 12
	},
	'pepper': {
		'texture': load("res://assets/food/raw/pepper.png"),
		'height': 14,
		'offset': 9
	},
	'riceball': {
		'texture': load("res://assets/food/raw/riceball.png"),
		'height': 14,
		'offset': 9
	},
	'nori': {
		'texture': load("res://assets/food/raw/seaweed.png"),
		'height': 6,
		'offset': 12
	}
}
const MEOWS = [
	'Meow01',
	'Meow02',
	'Meow03',
	'Meow04',
	'Meow05',
	'Meow06',
	'Meow07',
	'Meow08',
	'Meow09',
	'Meow10',
	'Meow11'
]


onready var textures = [
	load("res://assets/cats/og_cat.png"),
	load("res://assets/cats/scottish_cat.png"),
	load("res://assets/cats/spotty_cat.png"),
	load("res://assets/cats/star_cat.png"),
	load("res://assets/cats/stripe_cat.png"),
	load("res://assets/cats/swirl_cat.png")
]
onready var info_faces = {
	'okay': load("res://assets/ui/catface0.png"),
	'good': load("res://assets/ui/catface1.png"),
	'awesome': load("res://assets/ui/catface2.png")
}

var amount_to_eat
var least_amount_to_eat
var amount_ingredients
var seat_no = ''
var sushi = null
var table = null
var satisfaction = SATISFACTION_MAX
var maxSatisfaction = satisfaction
var sadOnce = false
var angryOnce = false
var isEating = false
var isSitting = false
var amount_eaten = 0
var amount_maki_eaten = 0
var amount_nigiri_eaten = 0
var favorites = {
	'type': 'maki',
	'main_ingredient': null,
	'side_ingredient_1': null,
	'side_ingredient_2': null,
	'nori': false
}


func _ready():
	randomize()
	self.set_texture(textures[randi() % textures.size()])
	amount_to_eat = randi() % (MAX_AMOUNT_TO_EAT - MIN_AMOUNT_TO_EAT) + MIN_AMOUNT_TO_EAT
	least_amount_to_eat = floor(amount_to_eat / 2)
	amount_ingredients = (randi() % 3) + 1
	if amount_ingredients > 0:
		favorites.main_ingredient = INGREDIENT_NAMES[randi() % INGREDIENT_NAMES.size()]
	if amount_ingredients > 1:
		favorites.side_ingredient_1 = _get_ingredient()
	if amount_ingredients > 2:
		favorites.side_ingredient_2 = _get_ingredient()
	if randi() % 2 == 0:
		favorites.nori = true
	if randi() % 2 == 0:
		favorites.type = 'nigiri'


func _get_ingredient() -> String:
	var ingredient = INGREDIENT_NAMES[randi() % INGREDIENT_NAMES.size()]
	while (ingredient == favorites.main_ingredient
				|| ingredient == favorites.side_ingredient_1
				|| ingredient == favorites.side_ingredient_2):
		ingredient = INGREDIENT_NAMES[randi() % INGREDIENT_NAMES.size()]
	return ingredient


func _physics_process(_delta):
	if randi() % 10000 < 4:
		get_node('Sound/' + MEOWS[randi() % MEOWS.size()]).play()


func _on_SatisfactionTimer_timeout():
	satisfaction -= 1
	if satisfaction <= SATISFACTION_SAD:
		if least_amount_to_eat <= _get_amount_eaten():
			GameManager.add_rating(get_rating())
			GameManager.add_money(get_money(true))
			queue_free()
			return
		if not sadOnce:
			sadOnce = true
	if satisfaction <= SATISFACTION_ANGRY:
		if not angryOnce:
			maxSatisfaction = SATISFACTION_SAD
			angryOnce = true
	if satisfaction == 0:
		GameManager.add_rating(get_rating())
		GameManager.add_money(get_money(true))
		queue_free()


func _evaluate_sushi():
	var points = 0
	match sushi.type:
		'maki':
			match favorites.main_ingredient:
				sushi.main_ingredient:
					points += 8
				sushi.side_ingredient_1:
					points += 4
				sushi.side_ingredient_2:
					points += 4
			match favorites.side_ingredient_1:
				null:
					points += 0
				sushi.main_ingredient:
					points += 2
				sushi.side_ingredient_1:
					points += 3
				sushi.side_ingredient_2:
					points += 3
			match favorites.side_ingredient_2:
				null:
					points += 0
				sushi.main_ingredient:
					points += 2
				sushi.side_ingredient_1:
					points += 3
				sushi.side_ingredient_2:
					points += 3
			if amount_ingredients == sushi.get_amount_ingredients():
				match amount_ingredients:
					1:
						points += 6
						if sushi.is_super_size:
							points += 10
					2:
						points += 9
					3:
						points += 6
			if favorites.type != 'maki':
				points = floor(points / 2)
		'nigiri':
			if favorites.main_ingredient == sushi.main_ingredient:
				points += 14
			elif favorites.side_ingredient_1 == sushi.main_ingredient:
				points += 7
			elif favorites.side_ingredient_2 == sushi.main_ingredient:
				points += 7
			if favorites.nori == sushi.has_nori:
				points += 6
			if favorites.type != 'nigiri':
				points = floor(points / 2)
	satisfaction = min(satisfaction + points, maxSatisfaction)
	_increase_amount_eaten()


func get_money(music := false) -> int:
	var money = (amount_maki_eaten * 5) + (amount_nigiri_eaten * 9)
	if angryOnce:
		return money
	var multiplier = satisfaction / (SATISFACTION_MAX / 1.5) + 1
	if sadOnce:
		multiplier = max(multiplier - 0.4, 1)
	if music:
		if multiplier <= 1.5:
			$Sound/Money_small.play()
		else:
			$Sound/Money_big.play()
	money *= multiplier
	return round(money) as int


func get_rating() -> float:
	if angryOnce:
		return 1.0
	var rating = round(satisfaction / (SATISFACTION_MAX / 8.0)) + 2
	if sadOnce:
		rating = max(rating - 1, 2)
	return rating / 2


func _get_amount_eaten() -> int:
	return amount_maki_eaten + amount_nigiri_eaten


func _increase_amount_eaten():
	if sushi.type == 'maki':
		amount_maki_eaten += 1
	else:
		amount_nigiri_eaten += 1
	if least_amount_to_eat <= _get_amount_eaten() && satisfaction <= SATISFACTION_SAD:
		GameManager.add_rating(get_rating())
		GameManager.add_money(get_money(true))
		queue_free()
		return
	if _get_amount_eaten() == amount_to_eat:
		GameManager.add_rating(get_rating())
		GameManager.add_money(get_money(true))
		queue_free()


func _get_correct_ingredients() -> Array:
	var correct_ingredients = []
	var completely_correct = 0
	if sushi.type == 'maki':
		match favorites.main_ingredient:
			sushi.main_ingredient:
				correct_ingredients.append(favorites.main_ingredient)
				completely_correct += 1
			sushi.side_ingredient_1, sushi.side_ingredient_2:
				correct_ingredients.append(favorites.main_ingredient)
		if favorites.side_ingredient_1:
			match favorites.side_ingredient_1:
				sushi.side_ingredient_1:
					correct_ingredients.append(favorites.side_ingredient_1)
					completely_correct += 1
				sushi.main_ingredient, sushi.side_ingredient_2:
					correct_ingredients.append(favorites.side_ingredient_1)
		if favorites.side_ingredient_2:
			match favorites.side_ingredient_2:
				sushi.side_ingredient_2:
					correct_ingredients.append(favorites.side_ingredient_2)
					completely_correct += 1
				sushi.main_ingredient, sushi.side_ingredient_1:
					correct_ingredients.append(favorites.side_ingredient_2)
		completely_correct = completely_correct == amount_ingredients
	elif sushi.type == 'nigiri':
		match sushi.main_ingredient:
			favorites.main_ingredient:
				correct_ingredients.append(favorites.main_ingredient)
				completely_correct += 1
			favorites.side_ingredient_1:
				correct_ingredients.append(favorites.side_ingredient_1)
			favorites.side_ingredient_2:
				correct_ingredients.append(favorites.side_ingredient_2)
		if sushi.has_nori && favorites.nori:
			correct_ingredients.append('nori')
			completely_correct += 1
		completely_correct = completely_correct == amount_ingredients + 1
	correct_ingredients.append(completely_correct)
	return correct_ingredients


func has_ingredient(newSushi) -> bool:
	if isEating:
		return false
	for ingredient in favorites:
		if favorites[ingredient] && ingredient != 'nori' && ingredient != 'type':
			if favorites[ingredient] == newSushi.main_ingredient:
				return true
			elif favorites[ingredient] == newSushi.side_ingredient_1:
				return true
			elif favorites[ingredient] == newSushi.side_ingredient_2:
				return true
	return false


func set_sushi(new_sushi):
	var new_pos = table.get_sushi_position(seat_no)
	sushi = new_sushi
	table.get_node('CustomerSort').add_child(sushi)
	sushi.position = new_pos
	$EatingTimer.start()
	$SatisfactionTimer.stop()
	$Sound/Munching.play()
	isEating = true
	
	var info_ingredients = _get_correct_ingredients()
	var completely_correct = info_ingredients.pop_back()
	if completely_correct:
		$Info/Smiley.texture = info_faces.awesome
	elif abs(info_ingredients.size() - amount_ingredients) >= 2:
		$Info/Smiley.texture = info_faces.okay
	else:
		$Info/Smiley.texture = info_faces.good
	for number in 3:
		var pos = (number + 1) as String
		var info_node = get_node("Info/Ingredient" + pos)
		if number < info_ingredients.size():
			info_node.texture = INGREDIENT_DATA[info_ingredients[number]].texture
		else:
			info_node.texture = null
	$Info.show()
	$InfoTimer.start()


func sitting_down(new_table, new_seat_no):
	table = new_table
	seat_no = new_seat_no
	$SatisfactionTimer.start()
	isSitting = true


func _on_EatingTimer_timeout():
	_evaluate_sushi()
	$SatisfactionTimer.start()
	sushi.queue_free()
	isEating = false


func _on_InfoTimer_timeout():
	$Info.hide()
